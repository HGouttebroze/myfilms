import React from 'react'
import { StyleSheet, View, TextInput, Button,  FlatList, ActivityIndicator } from 'react-native'
import FilmItem from './FilmItem'
import { getFilmsFromApiWithSearchedText } from '../API/TMDBApi' 

class Search extends React.Component {

  /*
 affichage des films de l'API, 
 les stocker dans une propriété de mon component. 
 RAPPEL: en React, on a pour habitude de définir nos propriétés dans le constructeur du component. ce n'est pas une obligation, mais c'est une bonne pratique.
 donc je réécris le constructeur du component,puis je créé une propriété  _films  et l'initialise avec un tableau 
  
  constructor(props) {
    super(props)
    this._film = []
  }
*/
  constructor(props) {
      super(props)
      this.searchedText = "" // Initialisation de notre donnée searchedText en dehors du state
      this.state = { 
        films: [],
  /*
  Pour 1 meilleur expérience utilisateur, je vais ajouter un indicateur de chargement:
  Je veut qu'à son chargement, avec  setState  , le component Search soit re-rendu pour afficher, ou non, le chargement.
  rajouter un booléen isLoading dans le state
  */ 
      isLoading: false // par defauts false car pas de chargement tant que recherche non lancée
      /*je le lancerais le chargement à TRUE plus bas lors du lancement du chargement loadfilms
     faut k j gére le lancement du chargement et son arrêt, soit lancer le chargement ds l appel à l'API et l'arrêter quand l'API retourne les films*/
      }
    }
_displayloading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
    ) 
  }
}
  _loadFilms() {
    //console.log(this.state.searchedText) // test log pr voir si j'ai bien la saisie du text ds le textInput
    if (this.searchedText.length > 0) // si le texte n'est pas vide
      this.setState({isLoading: true}) // lancement du chargement, ! RAPPEL: toujours passer par setState pour manipuler le state
      getFilmsFromApiWithSearchedText(this.searchedText).then(data => {
      this.setState({ 
        films: data.results,
        isLoading: false }) // arret du loading ou chargement
    //On va donc récupérer nos films dans le tableau "results"
    //La méthode forceUpdate() permet de forcer un component à se rendre à nouveau. Je vais de nouveau passer dans la méthode render de mon component. 
    //Cette fois  _films  contient les films de l'API et ma liste de données les affiche... 
    // ! la méthode  forceUpdate()  re-rend TOUT le component. Ici le TextInput, le Button et la FlatList sont re-rendus.
        //this.forceUpdate()
      })
  }
  _searchTextInputChanged(text) {
      this.searchedText = text // Modification du texte recherché à chaque saisie de texte, sans passer par le setState comme avant
    }

  

/*
J'initialisez le state:
Comme pour les variables d'un component, on a pour habitude d'initialiser le state dans le constructeur du component. Je vais 
initialiser le state avec un tableau de films vide 
*/
  
  render() {
    // avec setState, on coit que tout le component est re rendu avec le test sur le log
    // console.log("RENDER") 
    console.log(this.state.isLoading) // test log boolean is loading
    return (
      <View style={styles.main_container}>
        <TextInput 
        style={styles.textinput} 
        placeholder='Titre du film'
        onChangeText={(text) => this._searchTextInputChanged(text)}
        onSubmitEditing={() => this._loadFilms()}
        />
        <Button style={{ height: 50 }} title='Rechercher' onPress={() =>  this._loadFilms()}/>
        <FlatList
            data={this.state.films}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => <FilmItem film={item}/>}
        />
        {this._displayloading()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  loading_container: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center"
  },
  main_container: {
    flex: 1,
    marginTop: 30
    },
  textinput: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 10,
    height: 50,
    borderRadius: 5,
    borderColor: '#000000',
    borderWidth: 1,
    paddingLeft: 5
  },
})

export default Search

// Styles & Flex 
/*import React from 'react'
import { StyleSheet, View, TextInput, Button } from 'react-native'

class Search extends React.Component {
    render() {
        return (
            <View style={[styles.view, { backgroundColor: 'yellow'}]} >
                <View style={{ height: 335, backgroundColor: 'red' }}></View>
                <View style={{ height: 335, backgroundColor: 'green' }}></View>
                <TextInput style={styles.textinput} placeholder='Titre du disque'/>
                <Button style={styles.button} title='Rechercher' onPress={() => {}}/>
            </View>
)  
}
}

const styles = StyleSheet.create ({
view:  {
flex: 1,
marginTop: 30
},
textinput: {
marginLeft: 10,
marginRight: 17,
marginTop: 5,
marginBottom: 10,
height: 50,
borderColor: '#000000',
borderWidth: 1,
paddingLeft: 5

},

button: {
height: 50
}
})
export default Search
*/

//FlatList:
/*
<FlatList
  ItemSeparatorComponent={
    Platform.OS !== 'android' &&
    (({ highlighted }) => (
      <View
        style={[
          style.separator,
          highlighted && { marginLeft: 0 }
        ]}
      />
    ))
  }
  data={[{ title: 'Title Text', key: 'item1' }]}
  renderItem={({ item, index, separators }) => (
    <TouchableHighlight
      key={item.key}
      onPress={() => this._onPress(item)}
      onShowUnderlay={separators.highlight}
      onHideUnderlay={separators.unhighlight}>
      <View style={{ backgroundColor: 'white' }}>
        <Text>{item.title}</Text>
      </View>
    </TouchableHighlight>
  )}
/>
*/

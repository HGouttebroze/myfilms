import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'
import { getImageFromApi } from '../API/TMDBApi'


class FilmItem extends React.Component {
  render() {
    const film = this.props.film
    return (
      <View style={styles.main_container}>
        <Image style={styles.image}
        source={{uri: getImageFromApi(film.poster_path)}} />
        <View style={styles.content_container}>
          <View style={styles.content_header}>
            <Text style={styles.title_text}>{film.title}</Text>
            <Text style={styles.vote_text}>{film.vote_average}</Text>
          </View>
          <View style={styles.content_description}>
            <Text style={styles.desc_text} numberOfLines={6}>{film.overview}</Text>
          </View>
          <View style={styles.content_date}>
            <Text style={styles.date_text}>Sorti le {film.release_date}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: 190,

  },

  content_container: {
    flex: 1,
    margin: 5
  },

  content_header: {
    flex: 3,
    flexDirection: 'row'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5
  },
  vote_text: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#666666'
  },

  content_description: {
    flex: 7
  },
  desc_text: {
    fontStyle: 'italic',
    color: '#666666'
  },

  content_date: {
    flex: 1,
    
  },
  date_text: {
    textAlign: 'right',
    fontSize: 14
   
  },

  image: {
    width: 120,
    height: 180,
    margin: 5,
    backgroundColor: 'gray'
  },

});
   
export default FilmItem

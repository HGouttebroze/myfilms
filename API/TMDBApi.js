const API_TOKEN = "681ef6d1eaaed5d0a07e3a9c519c1a8d";

export function getFilmsFromApiWithSearchedText (text) {
    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text
    return fetch(url)
      .then((response) => response.json())
      .catch((error) => console.error(error))
  }
 
  export function getImageFromApi (name) {
    return 'https://image.tmdb.org/t/p/w300' + name
  }
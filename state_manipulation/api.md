# étape de la récupération d'un token sur l'API. 

Celui-ci se récupère en créant un compte et un projet sur l'API.

Le token, en français jeton d'authentification, est une clé de chiffres et de lettres permettant de vous identifier rapidement, sans avoir à saisir votre login / mot de passe. Avec ce token, l'API est capable de savoir rapidement que c'est M. X qui fait l'appel et qu'il a le droit de récupérer les films.

## utiliser l'API TMDB.

Première étape, la création d'un compte : https://www.themoviedb.org/account/signup?language=fr